<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
<!-- Required meta tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
<link rel="stylesheet" href="style.css">

<title>Calculadora IMC</title>
</head>

<body>
	<div class="container">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">Calculadora IMC</h5>
				<p class="card-text">A classificação do índice de massa corporal
					(IMC), pode ajudar a identificar obesidade ou desnutrição em
					crianças, adolescentes, adultos e idosos.</p>

				<form action="resultado.jsp">
					<div class="form-group">
						<label for="sex">Sexo</label> <select id="sex"
							class="form-control">
							<option selected>Selecione seu sexo...</option>
							<option>Homem</option>
							<option>Mulher</option>
						</select>
					</div>
					<div class="form-group">
						<label for="idade">Idade</label> <input type="number"
							class="form-control" id="idade" placeholder="25">
					</div>
					<div class="form-group">
						<label for="altura">Altura (m)</label> <input type="number"
							class="form-control" step="any" name="altura" placeholder="1,70">
					</div>
					<div class="form-group">
						<label for="peso">Peso (kg)</label> <input type="number"
							class="form-control" step="any" name="peso" placeholder="65">
					</div>
					<div class="form-group">
						<label for="atividade-fisica">Atividade Física</label> <select
							id="atividade-fisica" class="form-control">
							<option selected value=0>Selecione sua atividade física...</option>
							<option value=1>Sedentária</option>
							<option value=2>Moderada</option>
							<option value=3>Intensa</option>
						</select>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Calcular</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>

</html>